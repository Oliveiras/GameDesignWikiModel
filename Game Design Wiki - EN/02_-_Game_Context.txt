Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-05-14T13:13:22-03:00

====== Game Context ======

Describes the context in which your game takes place. It can be a medieval fantasy world or maybe a basketball competition. It should have the information needed to understand what is happening in the game.
Included in this section are:
[[+01 - Game Story]]
[[+02 - Previous Events]]
[[+03 - Main Characters]]
