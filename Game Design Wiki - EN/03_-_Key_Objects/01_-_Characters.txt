Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2016-05-14T13:29:57-03:00

====== Characters ======

In a slightly different way than the previous section, this one should describe every character involved with the game, such as enemies, NPCs and allies. If needed, separate pages may be created for each one of the characters.
See also: [[02 - Game Context:03 - Main Characters]]
